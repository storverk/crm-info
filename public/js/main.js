(function () {
    
})();
$(document).ready(function(){
    $(".owl-carousel").owlCarousel({
        items: 1,
        nav: true,
        dots: false,
        loop: true,
        navText: ''
    });
    
    $("#fixedAside").sticky({
        topSpacing:30,
        bottomSpacing: 1270
    });
    scroll();
    activeNav();
    $("#fixedAside").find('a').eq(0).addClass('active');

    $('.counter').waypoint(function (dir) {
        if (dir === 'down')
            $(this.element).find('p')
                .removeClass('fadeInDown hidden')
                .addClass('fadeInDown');
    }, {
        offset: '90%'
    });
});
$(window).resize(function () {
    $("#fixedAside").sticky('update');
});

function scroll() {
    var winW = $(window).innerWidth(),
        topPos = 35;
    if(winW < 993) {
        topPos = 60;
    } else if (winW >= 993) {
        topPos = 35;
    }
    $('#fixedAside').find('a').on('click', function() {
        var that = $(this),
            dataThat = that.attr('href');
        that.addClass('active').siblings().removeClass('active');
        $('body, html').animate({scrollTop: $(dataThat).offset().top-topPos}, 500);
    });
}
function activeNav() {
    $(window).scroll(function() {
        $('article').each(function () {
            var that = $(this),
                thatHeight = this.offsetHeight,
                mid = $(window).height()/2,
                actMid = $(window).scrollTop() + $(window).height()/2,
                dataThat = that.attr('id');

            if( $(window).scrollTop() < that.offset().top && that.offset().top < actMid){
                $('#fixedAside').find("a[href$="+dataThat+"]").addClass('active').siblings().removeClass('active');
            }
        })
    });
}