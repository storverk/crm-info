<?php

$prodRobots = <<<TEXT
User-agent: *
Disallow:

Host: http://1c-crm.info/
TEXT;

$devRobots = <<<TEXT
User-agent: *
Disallow: /
TEXT;

$env = 'prod';
if (is_file(__DIR__ . '/../env.php')) {
    $env = require (__DIR__ . '/../env.php');
}

header("Content-Type: text/plain");
if (in_array($env, ['dev', 'test'])) {
    echo $devRobots;
} else {
    echo $prodRobots;
}
